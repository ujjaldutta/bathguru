<?php
class SSTech_CancelOrder_OrderController
    extends Mage_Core_Controller_Front_Action
{
    public function preDispatch()
    {
        parent::preDispatch();

        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }

    /*
        @params for cancel the order along with email

    */
    
    public function cancelAction(){
        
        $order = Mage::getModel('sales/order')->load(
            $this->getRequest()->getParam('order_id')
        );
        if (!$order->getId()) {
           
             Mage::getSingleton('catalog/session')
                    ->addError($this->__('Order Does Not Exists.'));
              $this->_redirect('sales/order/history');
               return;
        }
        if (!$order->canCreditmemo()) {
           
               Mage::getSingleton('catalog/session')
                    ->addError($this->__('Cannot cancel your order.'));
               $this->_redirect('sales/order/history');
               return;
        }
        $data = array();

         
        $service = Mage::getModel('sales/service_order', $order);
       
        $creditmemo = $service->prepareCreditmemo($data);

        // refund to Store Credit
        if ($refundToStoreCreditAmount) {
           
            // check if refund to Store Credit is available
            if ($order->getCustomerIsGuest()) {
               
                  Mage::getSingleton('catalog/session')
                    ->addError($this->__('Cannot refund to store credit of your order.'));
            }
             
            $refundToStoreCreditAmount = max(
                0,     min($creditmemo->getBaseCustomerBalanceReturnMax(), $refundToStoreCreditAmount)
            );
          
            if ($refundToStoreCreditAmount) {
                
                $refundToStoreCreditAmount = $creditmemo->getStore()->roundPrice($refundToStoreCreditAmount);
                $creditmemo->setBaseCustomerBalanceTotalRefunded($refundToStoreCreditAmount);
                $refundToStoreCreditAmount = $creditmemo->getStore()->roundPrice(
                    $refundToStoreCreditAmount*$order->getStoreToOrderRate()
                );
                
                // this field can be used by customer balance observer
                $creditmemo->setBsCustomerBalTotalRefunded($refundToStoreCreditAmount);
                // setting flag to make actual refund to customer balance after credit memo save
                $creditmemo->setCustomerBalanceRefundFlag(true);
            }
        }
        $creditmemo->setPaymentRefundDisallowed(true)->register();
        // add comment to creditmemo
        
        if (!empty($comment)) {
            $creditmemo->addComment($comment, $notifyCustomer);
        }
        
        
        
        try {
            Mage::getModel('core/resource_transaction')
                ->addObject($creditmemo)
                ->addObject($order)
                ->save();
            // send email notification
            $creditmemo->sendEmail($notifyCustomer, ($includeComment ? $comment : ''));
            
            
            
            
            ///cancel the order
            
            
            $order->cancel();

                    if ($status = Mage::helper('cancelorder/customer')->getCancelStatus()) {
                        $order->addStatusHistoryComment('This order is cancelled by user. ', $status)
                              ->setIsCustomerNotified(1);
                    }

                    $order->save();
                     // If sending transactionnal email is enabled in system configuration, we send the email
                    //if(Mage::getStoreConfigFlag('sales/cancel/send_email')) {
                    $order->sendOrderUpdateEmail();
                    //}
                    $receiveEmail=Mage::getStoreConfig('trans_email/ident_sales/email');
                    $receiveName =Mage::getStoreConfig('trans_email/ident_sales/name');
                    $templateId ='3' ;
                    
                   $emailTemplate = Mage::getModel('core/email_template')->load($templateId);
                    $vars = array('customer' => Mage::getSingleton('customer/session')->getCustomer(), 'order' => $order,"subject"=>'Order os cancelled by user');
                     $processedTemplate =$emailTemplate->getProcessedTemplate($vars);
                    $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email'));

                     $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_general/name'));
                    $emailTemplate->send($receiveEmail,$receiveName, $vars);
            
           
            
            
            
            
            
            
            
             Mage::getSingleton('catalog/session')
                        ->addSuccess($this->__('Your order has been canceled.'));
        } catch (Mage_Core_Exception $e) {
             Mage::getSingleton('catalog/session')
                        ->addException($e, $this->__('Your order is invalid.'));
             
              
        }
         $this->_redirect('sales/order/history');

            return;
        
    }
    
    
    public function cancel2Action()
    {
        $order = Mage::getModel('sales/order')->load(
            $this->getRequest()->getParam('order_id')
        );

        if ($order->getId()) {
            if (Mage::helper('cancelorder/customer')->canCancel($order)) {
                try {
                    $order->cancel();

                    if ($status = Mage::helper('cancelorder/customer')->getCancelStatus()) {
                        $order->addStatusHistoryComment('', $status)
                              ->setIsCustomerNotified(1);
                    }

                    $order->save();
                     // If sending transactionnal email is enabled in system configuration, we send the email
                    //if(Mage::getStoreConfigFlag('sales/cancel/send_email')) {
                    $order->sendOrderUpdateEmail();
                    //}
                    $receiveEmail=Mage::getStoreConfig('trans_email/ident_sales/email');
                    $receiveName =Mage::getStoreConfig('trans_email/ident_sales/name');
                    $templateId ='3' ;
                    
                    $emailTemplate = Mage::getModel('core/email_template')->loadByCode($templateId);
                    $vars = array('customer' => Mage::getSingleton('customer/session')->getCustomer(), 'order' => $order,"subject"=>'Order os cancelled by user');
                    $emailTemplate->getProcessedTemplate($vars);
                    $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email'));

                     $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_general/name'));
                    $emailTemplate->send($receiveEmail,$receiveName, $vars);

                    Mage::getSingleton('catalog/session')
                        ->addSuccess($this->__('Your order has been canceled.'));
                } catch (Exception $e) {
                    Mage::getSingleton('catalog/session')
                        ->addException($e, $this->__('Cannot cancel your order.'));
                }
            } else {
                Mage::getSingleton('catalog/session')
                    ->addError($this->__('Cannot cancel your order.'));
            }

            $this->_redirect('sales/order/history');

            return;
        }

        $this->_forward('noRoute');
    }
}
