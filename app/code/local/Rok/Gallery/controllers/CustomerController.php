<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Module
 * @Author	Ujjal Dutta
 * @Author url	http://www.w3clouds.com
 * @eMail        <ujjal.dutta.pro@gmail.com>
 * @package     Mage_Connect
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Rok_Gallery_CustomerController extends Mage_Core_Controller_Front_Action
{

    /**
     * Check customer authentication
     */
    public function preDispatch()
    {
        parent::preDispatch();
        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }

    /**
     * Display downloadable links bought by customer
     *
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->setTitle(Mage::helper('downloadable')->__('My Gallery'));
        }
        
        if($this->getRequest()->getPost()){
        $post_data=$this->getRequest()->getPost();

        
        
                 if(isset($_FILES['galleryfile']['name']) && ($_FILES['galleryfile']['tmp_name'] != NULL))
                        {
                            $uploader = new Varien_File_Uploader('galleryfile');
                            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','flv','mp4')); 
                            $uploader->setAllowRenameFiles(false);
                            $uploader->setFilesDispersion(false);        
                            $path = Mage::getBaseDir('media') . DS ;
                            //$path= Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);     
                      
                            $path = $path ."gallery/gallery";
                            
                            $filename=uniqid().$_FILES['galleryfile']['name'];
                            $uploader->save($path, $filename);
                            //$userimage='gallery/gallery/'.$filename;
                            
                            $simage = Mage::getModel('gallery/gallery');
                            $simage->setData('user_id',Mage::getSingleton('customer/session')->getCustomer()->getId());
                            
                            $userimage="gallery/gallery/".$uploader->getUploadedFileName();
                            if($post_data['type']==0){
                            $simage->setData('image', $userimage);
                            }else{
                            $simage->setData('video', $userimage);
                                
                            }
                            
                            $simage->setData('category_id', $post_data['category']);
                            $simage->setData('title', $post_data['title']);
                            $simage->setData('type', $post_data['type']);
                            $simage->setData('pubdate',date('d-m-Y H:i:s'));
                          
                            $simage->setData('status',1);
                            $simage->save();
                          Mage::getSingleton('core/session')->addSuccess( $this->__('Medio is Successfully Save!') );
                          Mage::app()->getResponse()->setRedirect(Mage::getUrl('gallery/customer/index')); 
                      
                          }else{
                      
                              Mage::getSingleton('core/session')->addError( $this->__('Image Saving Error!') );
                              Mage::app()->getResponse()->setRedirect(Mage::getUrl('gallery/customer/index/')); 
                      
                          }
    
        }
    
    
        $this->renderLayout();
    }
    
    public function videoAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->setTitle(Mage::helper('downloadable')->__('My Gallery - Video'));
        }
        $this->renderLayout();
    }
    
    public function photoAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->setTitle(Mage::helper('downloadable')->__('My Gallery - Photo'));
        }
        $this->renderLayout();
    }
    
   

}
