<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
create table gallery(gallery_id int not null auto_increment, 
user_id int(11) NOT NULL,
category_id int(11) NOT NULL,
title varchar(255) NOT NULL,
image varchar(255) NOT NULL,
video varchar(255) NOT NULL,
details text NOT NULL,
url varchar(255) NOT NULL,
pubdate date NOT NULL,
public_display int(11) NOT NULL,
type int(11) NOT NULL,
status int (11) NOT NULL,
 primary key(gallery_id));

create table gallerycategory(category_id int not null auto_increment, 
user_id int(11) NOT NULL,
title varchar(255) NOT NULL,
pubdate date NOT NULL,
public_display int(11) NOT NULL,
status int (11) NOT NULL,
 primary key(category_id));
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 