<?php   
class Rok_Gallery_Block_Video extends Mage_Core_Block_Template{   


public function __construct()
    {
        parent::__construct();
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $collection = Mage::getModel('gallery/gallery')->getCollection()
        ->addFieldToFilter("type",1)
        ->addFieldToFilter("user_id",$customerData->getId());
        $this->setCollection($collection);
    }
 
   
 
   protected function _prepareLayout()
    {
        parent::_prepareLayout();
 
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,'all'=>'all'));
        $pager->setCollection($this->getCollection());
       
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
       
        return $this;
    }
 
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }


}