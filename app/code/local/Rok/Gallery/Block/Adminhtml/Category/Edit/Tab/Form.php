<?php
class Rok_Gallery_Block_Adminhtml_Category_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("gallery_form", array("legend"=>Mage::helper("gallery")->__("Item information")));

								
						/* $fieldset->addField('user_id', 'select', array(
						'label'     => Mage::helper('gallery')->__('User'),
						'values'   => Rok_Gallery_Block_Adminhtml_Category_Grid::getValueArray11(),
						'name' => 'user_id',
						));*/
						$fieldset->addField("title", "text", array(
						"label" => Mage::helper("gallery")->__("Category Name"),
						"name" => "title",
						));
					
						$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
							Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
						);

						$fieldset->addField('pubdate', 'date', array(
						'label'        => Mage::helper('gallery')->__('Create Date'),
						'name'         => 'pubdate',
						'time' => true,
						'image'        => $this->getSkinUrl('images/grid-cal.gif'),
						'format'       => $dateFormatIso
						));				
						 $fieldset->addField('public_display', 'select', array(
						'label'     => Mage::helper('gallery')->__('Public'),
						'values'   => Rok_Gallery_Block_Adminhtml_Category_Grid::getValueArray14(),
						'name' => 'public_display',
						));				
						 $fieldset->addField('status', 'select', array(
						'label'     => Mage::helper('gallery')->__('Status'),
						'values'   => Rok_Gallery_Block_Adminhtml_Category_Grid::getValueArray15(),
						'name' => 'status',
						));

				if (Mage::getSingleton("adminhtml/session")->getCategoryData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getCategoryData());
					Mage::getSingleton("adminhtml/session")->setCategoryData(null);
				} 
				elseif(Mage::registry("category_data")) {
				    $form->setValues(Mage::registry("category_data")->getData());
				}
				return parent::_prepareForm();
		}
}
