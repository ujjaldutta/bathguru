<?php

class Rok_Gallery_Block_Adminhtml_Category_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("categoryGrid");
				$this->setDefaultSort("category_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("gallery/category")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("category_id", array(
				"header" => Mage::helper("gallery")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "category_id",
				));
                
						/*$this->addColumn('user_id', array(
						'header' => Mage::helper('gallery')->__('User'),
						'index' => 'user_id',
						'type' => 'options',
						'options'=>Rok_Gallery_Block_Adminhtml_Category_Grid::getOptionArray11(),				
						));*/
						
				$this->addColumn("title", array(
				"header" => Mage::helper("gallery")->__("Category Name"),
				"index" => "title",
				));
					$this->addColumn('pubdate', array(
						'header'    => Mage::helper('gallery')->__('Create Date'),
						'index'     => 'pubdate',
						'type'      => 'datetime',
					));
						$this->addColumn('public_display', array(
						'header' => Mage::helper('gallery')->__('Public'),
						'index' => 'public_display',
						'type' => 'options',
						'options'=>Rok_Gallery_Block_Adminhtml_Category_Grid::getOptionArray14(),				
						));
						
						$this->addColumn('status', array(
						'header' => Mage::helper('gallery')->__('Status'),
						'index' => 'status',
						'type' => 'options',
						'options'=>Rok_Gallery_Block_Adminhtml_Category_Grid::getOptionArray15(),				
						));
						
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('category_id');
			$this->getMassactionBlock()->setFormFieldName('category_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_category', array(
					 'label'=> Mage::helper('gallery')->__('Remove Category'),
					 'url'  => $this->getUrl('*/adminhtml_category/massRemove'),
					 'confirm' => Mage::helper('gallery')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray11()
		{
            $data_array=array(); 
			//$data_array[0]='user1';
			//$data_array[1]='user2';
			
			$collection = Mage::getModel('customer/customer')->getCollection()
								->addAttributeToSelect('firstname')
								->addAttributeToSelect('lastname')
								->addAttributeToSelect('email');
								
			foreach ($collection as $item)
				{
				   $customer=$item->getData();
				   $data_array[$customer['entity_id']]=$customer['firstname']." ".$customer['lastname'];
				  
				}					
            return($data_array);
		}
		static public function getValueArray11()
		{
            $data_array=array();
			foreach(Rok_Gallery_Block_Adminhtml_Category_Grid::getOptionArray11() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray14()
		{
            $data_array=array(); 
			$data_array[0]='No';
			$data_array[1]='Yes';
            return($data_array);
		}
		static public function getValueArray14()
		{
            $data_array=array();
			foreach(Rok_Gallery_Block_Adminhtml_Category_Grid::getOptionArray14() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray15()
		{
            $data_array=array(); 
			$data_array[0]='Disabled';
			$data_array[1]='Enabled';
            return($data_array);
		}
		static public function getValueArray15()
		{
            $data_array=array();
			foreach(Rok_Gallery_Block_Adminhtml_Category_Grid::getOptionArray15() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}