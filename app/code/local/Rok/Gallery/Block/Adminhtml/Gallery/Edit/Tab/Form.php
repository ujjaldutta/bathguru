<?php
class Rok_Gallery_Block_Adminhtml_Gallery_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("gallery_form", array("legend"=>Mage::helper("gallery")->__("Item information")));

								
						 /*$fieldset->addField('user_id', 'select', array(
						'label'     => Mage::helper('gallery')->__('User'),
						'values'   => Rok_Gallery_Block_Adminhtml_Gallery_Grid::getValueArray0(),
						'name' => 'user_id',					
						"class" => "required-entry",
						"required" => true,
						));*/				
						 $fieldset->addField('category_id', 'select', array(
						'label'     => Mage::helper('gallery')->__('Category'),
						'values'   => Rok_Gallery_Block_Adminhtml_Gallery_Grid::getValueArray1(),
						'name' => 'category_id',					
						"class" => "required-entry",
						"required" => true,
						));
						$fieldset->addField("title", "text", array(
						"label" => Mage::helper("gallery")->__("Title"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "title",
						));
									
						$fieldset->addField('image', 'image', array(
						'label' => Mage::helper('gallery')->__('Image'),
						'name' => 'image',
						'note' => '(*.jpg, *.png, *.gif)',
						));				
						$fieldset->addField('video', 'file', array(
						'label' => Mage::helper('gallery')->__('Video'),
						'name' => 'video',
						'note' => '(*.mp4, *.flv)',
						));
						$fieldset->addField("details", "textarea", array(
						"label" => Mage::helper("gallery")->__("Details"),
						"name" => "details",
						));
					
						$fieldset->addField("url", "text", array(
						"label" => Mage::helper("gallery")->__("Video Url"),
						"name" => "url",
						));
					
						$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
							Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
						);

						$fieldset->addField('pubdate', 'date', array(
						'label'        => Mage::helper('gallery')->__('Publish Date'),
						'name'         => 'pubdate',					
						"class" => "required-entry",
						"required" => true,
						'time' => true,
						'image'        => $this->getSkinUrl('images/grid-cal.gif'),
						'format'       => $dateFormatIso
						));				
						 $fieldset->addField('public_display', 'select', array(
						'label'     => Mage::helper('gallery')->__('Public'),
						'values'   => Rok_Gallery_Block_Adminhtml_Gallery_Grid::getValueArray8(),
						'name' => 'public_display',					
						"class" => "required-entry",
						"required" => true,
						));
						 $fieldset->addField('type', 'select', array(
						'label'     => Mage::helper('gallery')->__('Type'),
						
						'type' => 'options',
						'name' => 'type',
						'values'=>Rok_Gallery_Block_Adminhtml_Gallery_Grid::getOptionArray9(),				
						));
						 
						 $fieldset->addField('status', 'select', array(
						'label'     => Mage::helper('gallery')->__('Status'),
						'values'   => Rok_Gallery_Block_Adminhtml_Gallery_Grid::getValueArray10(),
						'name' => 'status',
						));

				if (Mage::getSingleton("adminhtml/session")->getGalleryData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getGalleryData());
					Mage::getSingleton("adminhtml/session")->setGalleryData(null);
				} 
				elseif(Mage::registry("gallery_data")) {
				    $form->setValues(Mage::registry("gallery_data")->getData());
				}
				return parent::_prepareForm();
		}
}
