<?php

class Rok_Gallery_Block_Adminhtml_Gallery_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("galleryGrid");
				$this->setDefaultSort("gallery_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("gallery/gallery")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("gallery_id", array(
				"header" => Mage::helper("gallery")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "gallery_id",
				));
                
						/*$this->addColumn('user_id', array(
						'header' => Mage::helper('gallery')->__('User'),
						'index' => 'user_id',
						'type' => 'options',
						'options'=>Rok_Gallery_Block_Adminhtml_Gallery_Grid::getOptionArray0(),				
						));*/
						
						$this->addColumn('category_id', array(
						'header' => Mage::helper('gallery')->__('Category'),
						'index' => 'category_id',
						'type' => 'options',
						'options'=>Rok_Gallery_Block_Adminhtml_Gallery_Grid::getOptionArray1(),				
						));
						
				$this->addColumn("title", array(
				"header" => Mage::helper("gallery")->__("Title"),
				"index" => "title",
				));
				/*$this->addColumn("url", array(
				"header" => Mage::helper("gallery")->__("Video Url"),
				"index" => "url",
				));*/
					$this->addColumn('pubdate', array(
						'header'    => Mage::helper('gallery')->__('Publish Date'),
						'index'     => 'pubdate',
						'type'      => 'datetime',
					));
						$this->addColumn('public_display', array(
						'header' => Mage::helper('gallery')->__('Public'),
						'index' => 'public_display',
						'type' => 'options',
						'options'=>Rok_Gallery_Block_Adminhtml_Gallery_Grid::getOptionArray8(),				
						));
						
						$this->addColumn('type', array(
						'header' => Mage::helper('gallery')->__('Type'),
						'index' => 'type',
						'type' => 'options',
						'options'=>Rok_Gallery_Block_Adminhtml_Gallery_Grid::getOptionArray9(),				
						));
						
						$this->addColumn('status', array(
						'header' => Mage::helper('gallery')->__('Status'),
						'index' => 'status',
						'type' => 'options',
						'options'=>Rok_Gallery_Block_Adminhtml_Gallery_Grid::getOptionArray10(),				
						));
						
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('gallery_id');
			$this->getMassactionBlock()->setFormFieldName('gallery_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_gallery', array(
					 'label'=> Mage::helper('gallery')->__('Remove Gallery'),
					 'url'  => $this->getUrl('*/adminhtml_gallery/massRemove'),
					 'confirm' => Mage::helper('gallery')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray0()
		{
            $data_array=array(); 
			$data_array[0]='user1';
			$data_array[1]='user2';
            return($data_array);
		}
		static public function getValueArray0()
		{
            $data_array=array();
			foreach(Rok_Gallery_Block_Adminhtml_Gallery_Grid::getOptionArray0() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray1()
		{
            $data_array=array(); 
			//$data_array[0]='Category1';
			//$data_array[1]='Category2';
			$collection = Mage::getModel('gallery/category')->getCollection();
			foreach ($collection as $item)
				{
				   $category=$item->getData();
				   $data_array[$category['category_id']]=$category['title'];
				  
				}
            return($data_array);
		}
		static public function getValueArray1()
		{
            $data_array=array();
			foreach(Rok_Gallery_Block_Adminhtml_Gallery_Grid::getOptionArray1() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray8()
		{
            $data_array=array(); 
			$data_array[0]='No';
			$data_array[1]='Yes';
            return($data_array);
		}
		static public function getValueArray8()
		{
            $data_array=array();
			foreach(Rok_Gallery_Block_Adminhtml_Gallery_Grid::getOptionArray8() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray9()
		{
            $data_array=array(); 
			$data_array[0]='Image';
			$data_array[1]='Video';
            return($data_array);
		}
		static public function getValueArray9()
		{
            $data_array=array();
			foreach(Rok_Gallery_Block_Adminhtml_Gallery_Grid::getOptionArray9() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray10()
		{
            $data_array=array(); 
			$data_array[0]='Disabled';
			$data_array[1]='Enabled';
            return($data_array);
		}
		static public function getValueArray10()
		{
            $data_array=array();
			foreach(Rok_Gallery_Block_Adminhtml_Gallery_Grid::getOptionArray10() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}