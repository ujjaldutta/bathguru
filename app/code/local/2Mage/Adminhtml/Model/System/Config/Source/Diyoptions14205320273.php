<?php
class Mage_Adminhtml_Model_System_Config_Source_Diyoptions14205320273
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
		
            array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('Image Gallery')),
            array('value' => 2, 'label'=>Mage::helper('adminhtml')->__('Video Gallery')),
            array('value' => 3, 'label'=>Mage::helper('adminhtml')->__('Both')),
        );
    }

}
