<?php   
class Uipl_Contribution_Block_Earnings extends Mage_Core_Block_Template{   

 public function __construct()
    {
        parent::__construct();
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $collection = Mage::getModel('contribution/contribution')->getCollection()->addFieldToFilter("contributed_to",$customerData->getId());
        $this->setCollection($collection);
    }
 
   
 
   protected function _prepareLayout()
    {
        parent::_prepareLayout();
 
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,'all'=>'all'));
        $pager->setCollection($this->getCollection());
       
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
       
        return $this;
    }
 
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}