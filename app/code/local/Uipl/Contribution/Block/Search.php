<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Module
 * @Author	Ujjal Dutta
 * @Author url	http://www.w3clouds.com
 * @package     Mage_Connect
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Uipl_Contribution_Block_Search extends Mage_Core_Block_Template{   


 public function __construct()
    {
        parent::__construct();
        $collection=Mage::getResourceModel('customer/customer_collection')
        
        ->joinAttribute('billing_telephone', 'customer_address/telephone', 'default_billing', null, 'left')
        ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
       ;
        $collection->addAttributeToSelect('*');
       
        $this->setCollection($collection);
    }
 
   protected function _prepareLayout()
    {
        parent::_prepareLayout();
 
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,'all'=>'all'));
        $collection=$this->getCollection();
        $firstname=$this->getRequest()->getParam("firstname");
        $lastname=$this->getRequest()->getParam("lastname");
        $billing_telephone=$this->getRequest()->getParam("billing_telephone");
        $city=$this->getRequest()->getParam("city");
        $submit=$this->getRequest()->getParam("submit");
        if($firstname=='' || $lastname=='' || $billing_telephone=='' || $city==''){
         return $this;
        }
        
        $collection->addAttributeToFilter( array(
        array('attribute'=> 'firstname','like' => "%$firstname%")));
        $collection->addAttributeToFilter( array(
        array('attribute'=> 'lastname','like' => "%$lastname%")));
        $collection->addAttributeToFilter( array(
        array('attribute'=> 'billing_telephone','like' => "%$billing_telephone%")));
        $collection->addAttributeToFilter( array(
        array('attribute'=> 'billing_city','like' => "%$city%")));
        
        
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
       
        /*if(!isset($customerData['entity_id']) && isset($submit)){
         Mage::getSingleton('core/session')->addError('Please login to get wishlist search result.');
         $url = Mage::getUrl("/",array("_nosid"=>true));
        Mage::app()->getFrontController()->getResponse()->setRedirect($url);
        
        }*/
        
        if(isset($customerData['entity_id'])){
        $collection->addFieldToFilter("entity_id",array("neq"=>$customerData['entity_id']));
        }
        
        $collection->getSelect()
                ->joinLeft(array('wishlist' => 'wishlist'),
                       '`e`.entity_id = wishlist.customer_id '
                );
     
        $collection->getSelect()->where(new Zend_Db_Expr("(wishlist.customer_id > 0)"));
        
        $this->setCollection($collection);
        $pager->setCollection($this->getCollection());
        
        if($collection->count()<=0){
         Mage::getSingleton('core/session')->addError('No record found for wishlist contribution.');
	   
          Mage::app()->getResponse()->setRedirect(Mage::getUrl("contribution/index/search"))->sendResponse();
        }
        $data=$collection->getData();
       
        $wishlisturl=Mage::getUrl('contribution/index/index',array("id"=>$data[0]['entity_id']));
        
        Mage::app()->getResponse()->setRedirect($wishlisturl)->sendResponse();
     // echo  $collection->getSelect()->__toString();exit;
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
       
        return $this;
    }
 
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}