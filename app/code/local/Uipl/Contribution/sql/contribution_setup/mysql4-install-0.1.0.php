<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE `contribution_wishlist` (
`list_id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`user_id` INT( 11 ) NOT NULL ,
`contributed_to` INT( 11 ) NOT NULL ,
`product_id` INT( 11 ) NOT NULL ,
`cdate` DATETIME NOT NULL ,
`amount` FLOAT NOT NULL,
`transaction_id` varchar(255)  NOT NULL,
  `gateway` varchar(255)  NOT NULL
) ;
CREATE TABLE `contribution_wallet` (
`wallet_id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`user_id` INT( 11 ) NOT NULL ,
`amount` FLOAT NOT NULL
) ;
CREATE TABLE `contribution_redeemtion` (
`redeem_id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`user_id` INT( 11 ) NOT NULL ,
`amount` FLOAT NOT NULL,
`order_id`  INT( 11 ) NOT NULL ,
`redeem_date` DATETIME NOT NULL
) ;
		
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 