<?php

class Uipl_Contribution_Model_Observer extends Mage_Core_Model_Abstract
{
   
    
public function setDiscount($observer)
    {
     //apply discount redeemption to cart
     
       $quote=$observer->getEvent()->getQuote();
       $quoteid=$quote->getId();
       $discountAmount= Mage::getSingleton('core/session')->getRedeemAmt();
	  if($quoteid) {
       
       
      
		if($discountAmount>0) {
		   $total=$quote->getBaseSubtotal();
		    $quote->setSubtotal(0);
		    $quote->setBaseSubtotal(0);
		 
		    $quote->setSubtotalWithDiscount(0);
		    $quote->setBaseSubtotalWithDiscount(0);
		 
		    $quote->setGrandTotal(0);
		    $quote->setBaseGrandTotal(0);
		   
		     
		    $canAddItems = $quote->isVirtual()? ('billing') : ('shipping'); 
		    foreach ($quote->getAllAddresses() as $address) {
		     
		    $address->setSubtotal(0);
			     $address->setBaseSubtotal(0);
		 
			     $address->setGrandTotal(0);
			     $address->setBaseGrandTotal(0);
		 
			     $address->collectTotals();
		 
			     $quote->setSubtotal((float) $quote->getSubtotal() + $address->getSubtotal());
			     $quote->setBaseSubtotal((float) $quote->getBaseSubtotal() + $address->getBaseSubtotal());
		 
			     $quote->setSubtotalWithDiscount(
				 (float) $quote->getSubtotalWithDiscount() + $address->getSubtotalWithDiscount()
			     );
			     $quote->setBaseSubtotalWithDiscount(
				 (float) $quote->getBaseSubtotalWithDiscount() + $address->getBaseSubtotalWithDiscount()
			     );
		 
			     $quote->setGrandTotal((float) $quote->getGrandTotal() + $address->getGrandTotal());
			     $quote->setBaseGrandTotal((float) $quote->getBaseGrandTotal() + $address->getBaseGrandTotal());
		  
		    $quote ->save(); 
		  
		       $quote->setGrandTotal($quote->getBaseSubtotal()-$discountAmount)
		       ->setBaseGrandTotal($quote->getBaseSubtotal()-$discountAmount)
		       ->setSubtotalWithDiscount($quote->getBaseSubtotal()-$discountAmount)
		       ->setBaseSubtotalWithDiscount($quote->getBaseSubtotal()-$discountAmount)
		       ->save(); 
		       
		     
		     if($address->getAddressType()==$canAddItems) {
		     //echo $address->setDiscountAmount; exit;
		      $address->setSubtotalWithDiscount((float) $address->getSubtotalWithDiscount()-$discountAmount);
		      $address->setGrandTotal((float) $address->getGrandTotal()-$discountAmount);
		      $address->setBaseSubtotalWithDiscount((float) $address->getBaseSubtotalWithDiscount()-$discountAmount);
		      $address->setBaseGrandTotal((float) $address->getBaseGrandTotal()-$discountAmount);
		      if($address->getDiscountDescription()){
		      $address->setDiscountAmount(-($address->getDiscountAmount()-$discountAmount));
		      $address->setDiscountDescription('Redeemed');
		      $address->setBaseDiscountAmount(-($address->getBaseDiscountAmount()-$discountAmount));
		      }else {
		      $address->setDiscountAmount(-($discountAmount));
		      $address->setDiscountDescription('Redeemed');
		      $address->setBaseDiscountAmount(-($discountAmount));
		      }
		      
		      /*$address->addTotal(array(
			      'code' => 'Redeem',
			      'title' => 'Redeem',
			      'value' => $discountAmount
			  ));*/
		      $address->save();
		     }//end: if
		    } //end: foreach
		    //echo $quote->getGrandTotal();
		   
		   foreach($quote->getAllItems() as $item){
				  //We apply discount amount based on the ratio between the GrandTotal and the RowTotal
				  $rat=$item->getPriceInclTax()/$total;
				  $ratdisc=$discountAmount*$rat;
				  $item->setDiscountAmount(($item->getDiscountAmount()+$ratdisc) * $item->getQty());
				  $item->setBaseDiscountAmount(($item->getBaseDiscountAmount()+$ratdisc) * $item->getQty())->save();
				 
				}
			     
				 
	       }
	      // Mage::getSingleton('core/session')->unsRedeemAmt();
            
	  }
 }
 

public function setRedeem($observer)
    {
	  
	  $customerData = Mage::getSingleton('customer/session')->getCustomer();
	  //$cdata=Mage::getSingleton('core/session')->getContributeTo();
	  $amount=(float)Mage::getSingleton('core/session')->getRedeemAmt();
          if($amount<=0){
              return false;
          }
	  $order = new Mage_Sales_Model_Order();
	  $incrementId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
	  $order->loadByIncrementId($incrementId); 
	  $id = $order->getId();


	  // deduct amount and send email to user and admin	  
	  $emailTemplate  = Mage::getModel('core/email_template')->loadDefault('redeem_email_template');
	  $emailTemplateVariables = array();
	  $emailTemplateVariables['orderid'] = '#'.$id;
	  $emailTemplateVariables['redeemamount'] = $amount;
	  $emailTemplateVariables['orderamount'] =Mage::helper('core')->currency($order->getBaseGrandTotal(), true, false);
	  $emailTemplateVariables['customer']=$customerData; 
	  $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
	  $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email'));
 	  $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_general/name'));
	  // Mage::log($processedTemplate);
	  $emailTemplate->send($customerData->getEmail(),$customerData->getFirstname()." ".$customerData->getLastname(), $emailTemplateVariables);
	  $emailTemplate->send(Mage::getStoreConfig('trans_email/ident_sales/email'),Mage::getStoreConfig('trans_email/ident_sales/name'), $emailTemplateVariables);


	  
	  $wconn = Mage::getSingleton('core/resource')->getConnection('core_write');
	  
	  $data = array('user_id'=>$customerData->getId(),'amount'=>$amount,'redeem_date'=>date("Y-m-d"),'order_id'=>$id);
	  $model = Mage::getModel('contribution/redeemtion')->setData($data);
	  $model->save();
	 // echo "update contribution_wallet set amount=amount-".$amount." where user_id=".$customerData->getId();
          $wconn->query("update contribution_wallet set amount=amount-".$amount." where user_id=".$customerData->getId());
	  
	  Mage::getSingleton('core/session')->unsRedeemAmt();
	  
    }
    
    
}
	