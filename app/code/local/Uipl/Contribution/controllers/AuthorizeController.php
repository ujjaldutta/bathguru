<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Module
 * @Author	Ujjal Dutta
 * @Author url	http://www.w3clouds.com
 * @eMail        <ujjal.dutta.pro@gmail.com>
 * @package     Mage_Connect
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Uipl_Contribution_AuthorizeController extends Mage_Core_Controller_Front_Action{
    
   
    public function processAction(){
	
	if (Mage::getSingleton('customer/session')->isLoggedIn()==0){
	    $session = $this->_getSession();
	    $referer=Mage::getUrl("contribution/index/search");
	    $session->setBeforeAuthUrl($referer);
	    
	    $this->_redirectUrl(Mage::getUrl("customer/account/login"));
	  }
	
	$this->loadLayout();  
	
	if($_POST) //Post Data received from product list page.
	{
	    //Mainly we need 4 variables from product page Item Name, Item Price, Item Number and Item Quantity.
	   
	    //Please Note : People can manipulate hidden field amounts in form,
	    //In practical world you must fetch actual price from database using item id. Eg:
	    //$ItemPrice = $mysqli->query("SELECT item_price FROM products WHERE id = Product_Number");
	
	    $ItemName       = $this->getRequest()->getPost("itemname"); //Item Name
	    $ItemPrice      = $this->getRequest()->getPost("itemprice"); //Item Price
	    $ItemNumber     = $this->getRequest()->getPost("itemnumber"); //Item Number
	    $ItemDesc       = $this->getRequest()->getPost("itemdesc"); //Item description
	    $ItemQty        = $this->getRequest()->getPost("itemQty"); // Item Quantity
	    $firstname	    = $this->getRequest()->getPost("firstname"); 
	    $lastname	    = $this->getRequest()->getPost("lastname");
	    $street	    = $this->getRequest()->getPost("street");
	    $state	    = $this->getRequest()->getPost("state");
	    $zip	    = $this->getRequest()->getPost("zip");
	    $ItemTotalPrice = ($ItemPrice*$ItemQty); //(Item Price x Quantity = Total) Get total amount of product;
	    
	    //credit card details
	    $card_no=$this->getRequest()->getPost("cc_number");
	    $exp_month=$this->getRequest()->getPost("exp_month");
	    if($exp_month<9)
	    {
		$exp_month='0'.$exp_month;
	    }
	    $exp_year=$this->getRequest()->getPost("exp_year");
	    $exp_year=substr($exp_year,2,2);
	   

		$post_url =  Mage::getStoreConfig('payment/authorizenet/cgi_url');
		
		$post_values = array(
			
			// the API Login ID and Transaction Key must be replaced with valid values
			"x_login"		=> Mage::getStoreConfig('payment/authorizenet/login'),
			"x_tran_key"		=> Mage::getStoreConfig('payment/authorizenet/trans_key'),
		
			"x_version"		=> "3.1",
			"x_delim_data"		=> "TRUE",
			"x_delim_char"		=> "|",
			"x_relay_response"	=> "FALSE",
		
			"x_type"		=> "AUTH_CAPTURE",
			"x_method"		=> "CC",
			"x_card_num"		=> $card_no,
			"x_exp_date"		=> $exp_month.$exp_year,
		
			"x_amount"		=> $ItemTotalPrice,
			"x_description"		=> $ItemDesc,
		
			"x_first_name"		=> $firstname,
			"x_last_name"		=> $lastname,
			"x_address"		=> $street,
			"x_state"		=> $state,
			"x_zip"			=> $zip
			// Additional fields can be added here as outlined in the AIM integration
			// guide at: http://developer.authorize.net
		);
		
		// This section takes the input fields and converts them to the proper format
		// for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
		$post_string = "";
		foreach( $post_values as $key => $value )
			{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
		$post_string = rtrim( $post_string, "& " );
		
		// The following section provides an example of how to add line item details to
		// the post string.  Because line items may consist of multiple values with the
		// same key/name, they cannot be simply added into the above array.
		//
		// This section is commented out by default.
		/*
		$line_items = array(
			"item1<|>golf balls<|><|>2<|>18.95<|>Y",
			"item2<|>golf bag<|>Wilson golf carry bag, red<|>1<|>39.99<|>Y",
			"item3<|>book<|>Golf for Dummies<|>1<|>21.99<|>Y");
			
		foreach( $line_items as $value )
			{ $post_string .= "&x_line_item=" . urlencode( $value ); }
		*/
		
		// This sample code uses the CURL library for php to establish a connection,
		// submit the post, and record the response.
		// If you receive an error, you may want to ensure that you have the curl
		// library enabled in your php configuration
		$request = curl_init($post_url); // initiate curl object
			curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
			curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
			curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
			$post_response = curl_exec($request); // execute curl post and store results in $post_response
			// additional options may be required depending upon your server configuration
			// you can find documentation on curl options at http://www.php.net/curl_setopt
		curl_close ($request); // close curl object
		
		// This line takes the response and breaks it into an array using the specified delimiting character
		$response_array = explode($post_values["x_delim_char"],$post_response);
		
		// The results are output to the screen in the form of an html numbered list.
		//echo "<OL>\n";
		foreach ($response_array as $value)
		{
			//echo "<LI>" . $value . "&nbsp;</LI>\n";
		}
		//echo "</OL>\n";
		
		
		    if($response_array[2]==1){
		    
		    
		    $customerData = Mage::getSingleton('customer/session')->getCustomer();
				$cdata=Mage::getSingleton('core/session')->getContributeTo();
				
				$data = array('user_id'=>$customerData->getId(),'contributed_to'=>$cdata['entity_id'],'cdate'=>date("Y-m-d"),"amount"=>urldecode($response_array[9]),"transaction_id"=>urldecode($response_array[37]),"gateway"=>"authorize");
				$model = Mage::getModel('contribution/contribution')->setData($data);
				$model->save();
				/*try {
				$insertId = ->getId();
			       
				} catch (Exception $e){
				
				}*/
				$conn = Mage::getSingleton('core/resource')->getConnection('core_read'); 
				$wconn = Mage::getSingleton('core/resource')->getConnection('core_write');
				// perform sql queries
				$result = $conn->fetchAll("SELECT * FROM contribution_wallet where user_id=".$cdata['entity_id']);
			       
				if(count($result)<=0){
				$wdata = array('user_id'=>$cdata['entity_id'],"amount"=>urldecode($response_array[9]));
				$model = Mage::getModel('contribution/wallet')->setData($wdata);
				$model->save();
				    
				}else{
				//	$wdata = array("amount"=>urldecode($httpParsedResponseAr["PAYMENTINFO_0_AMT"])+$result['amount']);
				//$model = Mage::getModel('contribution/wallet')->load()->addFieldToFilter("user_id",array('eq' => $cdata['entity_id']))->addData($wdata);
				//$model->save();
				$wconn->query("update contribution_wallet set amount=amount+".urldecode($response_array[9])." where user_id=".$cdata['entity_id']);
				}
				
			Mage::getSingleton('core/session')->addSuccess('Thanks for you contribution. Your payment is successful.');
			$this->_redirectUrl(Mage::getUrl("contribution/index/success"));
	       
		    }else{
			
			Mage::getSingleton('core/session')->addError('Your payment failed.');
			    $this->_redirectUrl(Mage::getUrl("contribution/index/cancel"));
		    }
	   	}
	$this->renderLayout(); 
	
    }
   
}