<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Module
 * @Author	Ujjal Dutta
 * @Author url	http://www.w3clouds.com
 * @package     Mage_Connect
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Uipl_Contribution_IndexController extends Mage_Core_Controller_Front_Action{
    
    public $PayPalMode;
    public $PayPalApiUsername;
    public $PayPalApiPassword;
    public $PayPalApiSignature;
    public $PayPalCurrencyCode;
    public $PayPalReturnURL;
    public $PayPalCancelURL;
    
   
    public function IndexAction() {
 
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Wishlist Contribution"));
	  
	  $active=Mage::getStoreConfig('contribution/settings/enabled');
	  if($active==0){
	    Mage::app()->getResponse()->setRedirect(Mage::getUrl("/"))->sendResponse();
	  }
	   if (!Mage::getSingleton('customer/session')->isLoggedIn()){
	   
	    $session = $this->_getSession();
	    $referer=Mage::getUrl("contribution/index",array("id"=>$this->getRequest()->getParam("id")));
	    $session->setBeforeAuthUrl($referer);
	    
	    $this->_redirectUrl(Mage::getUrl("customer/account/login"));
	  }
	  
	$id=$this->getRequest()->getParam("id");
	  
	  if($id==''){
	    Mage::getSingleton('core/session')->addError('Please search user from wishlist for contribution.');
	    $this->_redirectUrl(Mage::getUrl("contribution/index/search"));
	  }
	  
    $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
	 
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("wishlist contribution", array(
                "label" => $this->__("Wishlist Contribution"),
                "title" => $this->__("Wishlist Contribution")
		   ));
    
      $this->renderLayout(); 
	  
    }
    
    public function searchAction(){
	
	/*if (Mage::getSingleton('customer/session')->isLoggedIn()==0){
	    $session = $this->_getSession();
	    $referer=Mage::getUrl("contribution/index/search");
	    $session->setBeforeAuthUrl($referer);
	    
	    $this->_redirectUrl(Mage::getUrl("customer/account/login"));
	  }*/
	  
    
    $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Search Wishlist"));
	 $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
	 
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("wishlist contribution", array(
                "label" => $this->__("Search Wishlist"),
                "title" => $this->__("Search Wishlist")
		   ));
    
    

    
    
      $this->renderLayout(); 
    }
    
    public function createwishlistAction(){
	
	 $referer=Mage::getUrl("/");
	if (Mage::getSingleton('customer/session')->isLoggedIn()==0){
	    $session = $this->_getSession();
	   
	    $session->setBeforeAuthUrl($referer);
	    
	    $this->_redirectUrl(Mage::getUrl("customer/account/login"));
	  }else{
	   $this->_redirectUrl($referer);
	  
	  }
	
    }
    
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }
    
    public function payAction(){
	$this->PayPalMode         = (Mage::getStoreConfig('contribution/settings/gift_paypal_mode')==1)?"sandbox":""; // sandbox or live
	$this->PayPalApiUsername  = Mage::getStoreConfig('contribution/settings/gift_paypal_username'); //PayPal API Username
        $this->PayPalApiPassword  = Mage::getStoreConfig('contribution/settings/gift_paypal_password'); //Paypal API password
	$this->PayPalApiSignature = Mage::getStoreConfig('contribution/settings/gift_paypal_signature'); //Paypal API Signature
        $this->PayPalCurrencyCode = 'CAD'; //Paypal Currency Code
	$this->PayPalReturnURL    = Mage::getUrl("contribution/index/process"); //Point to process.php page
        $this->PayPalCancelURL    = Mage::getUrl("contribution/index/cancel"); //Cancel URL if
	
	
	if (Mage::getSingleton('customer/session')->isLoggedIn()==0){
	    $session = $this->_getSession();
	    $referer=Mage::getUrl("contribution/index/search");
	    $session->setBeforeAuthUrl($referer);
	    
	    $this->_redirectUrl(Mage::getUrl("customer/account/login"));
	  }
	  
	  $id=$this->getRequest()->getParam("id");
	  
	  if($id==''){
	    Mage::getSingleton('core/session')->addError('Please search user from wishlist for contribution.');
	    $this->_redirectUrl(Mage::getUrl("contribution/index/search"));
	  }
	  
	  
	  $customerData = Mage::getModel('customer/customer')->load($id)->getData();
	  Mage::register('customerData', $customerData);
	  Mage::getSingleton('core/session')->setContributeTo($customerData);
	  
	  $customerData = Mage::getSingleton('customer/session')->getCustomer();
	  
	  if($id==$customerData['entity_id']){
	    $this->_redirectUrl(Mage::getUrl("contribution/index/search"));
	    
	  }
	 $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Wishlist Contribution"));
	 $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
	 
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("wishlist contribution", array(
                "label" => $this->__("Wishlist Contribution"),
                "title" => $this->__("Wishlist Contribution")
		   ));
    
    

    
    
      $this->renderLayout(); 
    }
    
    public function processAction(){
	//paypal payment gateway process
           $active=Mage::getStoreConfig('payment/paypal_express/active');
       
        if($active!=1){
            
             $this->_redirectUrl(Mage::getUrl("/"));
        }
	
	$this->PayPalMode         = (Mage::getStoreConfig('contribution/settings/gift_paypal_mode')==1)?"sandbox":""; // sandbox or live
	$this->PayPalApiUsername  = Mage::getStoreConfig('contribution/settings/gift_paypal_username'); //PayPal API Username
        $this->PayPalApiPassword  = Mage::getStoreConfig('contribution/settings/gift_paypal_password'); //Paypal API password
	$this->PayPalApiSignature = Mage::getStoreConfig('contribution/settings/gift_paypal_signature'); //Paypal API Signature
        $this->PayPalCurrencyCode = 'CAD'; //Paypal Currency Code
	$this->PayPalReturnURL    = Mage::getUrl("contribution/index/process"); //Point to process.php page
        $this->PayPalCancelURL    = Mage::getUrl("contribution/index/cancel"); //Cancel URL if
	
	if (Mage::getSingleton('customer/session')->isLoggedIn()==0){
	    $session = $this->_getSession();
	    $referer=Mage::getUrl("contribution/index/search");
	    $session->setBeforeAuthUrl($referer);
	    
	    $this->_redirectUrl(Mage::getUrl("customer/account/login"));
	  }
	
	$this->loadLayout();  
	$paypalmode = ($this->PayPalMode=='sandbox') ? '.sandbox' : '';
	
	if($_POST) //Post Data received from product list page.
	{
	    
	
	    $ItemName       = $this->getRequest()->getPost("itemname"); //Item Name
	    $ItemPrice      = $this->getRequest()->getPost("itemprice"); //Item Price
	    $ItemNumber     = $this->getRequest()->getPost("itemnumber"); //Item Number
	    $ItemDesc       = $this->getRequest()->getPost("itemdesc"); //Item description
	    $ItemQty        = $this->getRequest()->getPost("itemQty"); // Item Quantity
	    $ItemTotalPrice = ($ItemPrice*$ItemQty); //(Item Price x Quantity = Total) Get total amount of product;
	   
	   
	    //Grand total including all tax, insurance, shipping cost and discount
	    //$GrandTotal = ($ItemTotalPrice + $TotalTaxAmount + $HandalingCost + $InsuranceCost + $ShippinCost + $ShippinDiscount);
	   $GrandTotal = $ItemTotalPrice;
	    //Parameters for SetExpressCheckout, which will be sent to PayPal
	    $padata =   '&METHOD=SetExpressCheckout'.
			'&RETURNURL='.urlencode($this->PayPalReturnURL ).
			'&CANCELURL='.urlencode($this->PayPalCancelURL).
			'&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").
		       
			'&L_PAYMENTREQUEST_0_NAME0='.urlencode($ItemName).
			'&L_PAYMENTREQUEST_0_NUMBER0='.urlencode($ItemNumber).
			'&L_PAYMENTREQUEST_0_DESC0='.urlencode($ItemDesc).
			'&L_PAYMENTREQUEST_0_AMT0='.urlencode($ItemPrice).
			'&L_PAYMENTREQUEST_0_QTY0='. urlencode($ItemQty).
		       
			
		       
			'&NOSHIPPING=0'. //set 1 to hide buyer's shipping address, in-case products that do not require shipping
		       
			'&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice).
			
			'&PAYMENTREQUEST_0_AMT='.urlencode($GrandTotal).
			'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($this->PayPalCurrencyCode).
			'&LOCALECODE=GB'. //PayPal pages to match the language on your website.
			//'&LOGOIMG='.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN)."images/logo.gif". //site logo.
			'&CARTBORDERCOLOR=FFFFFF'. //border color of cart
			'&ALLOWNOTE=1';
		       
			############# set session variable we need later for "DoExpressCheckoutPayment" #######
			Mage::getSingleton('core/session')->setItemName($ItemName);
			Mage::getSingleton('core/session')->setItemPrice($ItemPrice);
			Mage::getSingleton('core/session')->setItemNumber($ItemNumber);
			Mage::getSingleton('core/session')->setItemDesc($ItemDesc);
			Mage::getSingleton('core/session')->setItemQty($ItemQty);
			Mage::getSingleton('core/session')->setItemTotalPrice($ItemTotalPrice);
		
			Mage::getSingleton('core/session')->setGrandTotal($GrandTotal);
	
	
	
		
		$httpParsedResponseAr = $this->PPHttpPost('SetExpressCheckout', $padata, $this->PayPalApiUsername, $this->PayPalApiPassword, $this->PayPalApiSignature, $this->PayPalMode);
	   
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
		{
	
			
			$paypalurl ='https://www'.$paypalmode.'.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$httpParsedResponseAr["TOKEN"].'';
			$this->_redirectUrl($paypalurl);
		     
		}else{
		    
		    Mage::getSingleton('core/session')->addError('Your payment failed.');
		    $this->_redirectUrl(Mage::getUrl("contribution/index/cancel"));
		}
	
	}
	
	
	if(isset($_GET["token"]) && isset($_GET["PayerID"]))
	{
	
	   
	    $token = $_GET["token"];
	    $payer_id = $_GET["PayerID"];
	   
	    //get session variables
	    $ItemName           = Mage::getSingleton('core/session')->getItemName(); //Item Name
	    $ItemPrice          = Mage::getSingleton('core/session')->getItemPrice() ; //Item Price
	    $ItemNumber         = Mage::getSingleton('core/session')->getItemNumber(); //Item Number
	    $ItemDesc           = Mage::getSingleton('core/session')->getItemDesc(); //Item Number
	    $ItemQty            = Mage::getSingleton('core/session')->getItemQty(); // Item Quantity
	    $ItemTotalPrice     = Mage::getSingleton('core/session')->getItemTotalPrice(); //total amount of product;
	   
	    $GrandTotal         = Mage::getSingleton('core/session')->getGrandTotal();
	
	    $padata =   '&TOKEN='.urlencode($token).
			'&PAYERID='.urlencode($payer_id).
			'&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").
		       
			//set item info here, otherwise we won't see product details later 
			'&L_PAYMENTREQUEST_0_NAME0='.urlencode($ItemName).
			'&L_PAYMENTREQUEST_0_NUMBER0='.urlencode($ItemNumber).
			'&L_PAYMENTREQUEST_0_DESC0='.urlencode($ItemDesc).
			'&L_PAYMENTREQUEST_0_AMT0='.urlencode($ItemPrice).
			'&L_PAYMENTREQUEST_0_QTY0='. urlencode($ItemQty).
	
		
			'&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice).
			
			'&PAYMENTREQUEST_0_AMT='.urlencode($GrandTotal).
			'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($this->PayPalCurrencyCode);
	   
	   
	   
	    $httpParsedResponseAr = $this->PPHttpPost('DoExpressCheckoutPayment', $padata, $this->PayPalApiUsername, $this->PayPalApiPassword, $this->PayPalApiSignature, $this->PayPalMode);
	 
	    //Check if everything went ok..
	    if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
	    {
	
		   // echo '<h2>Success</h2>';
		    //echo 'Your Transaction ID : '.urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]);
		   
			
		       
			if('Completed' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"])
			{
			    //echo '<div style="color:green">Payment Received! Your product will be sent to you very soon!</div>';
			}
			elseif('Pending' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"])
			{
			   // echo '<div style="color:red">Transaction Complete, but payment is still pending! '.
			    //'You need to manually authorize this payment in your <a target="_new" href="http://www.paypal.com">Paypal Account</a></div>';
			}
	
			// we can retrive transection details using either GetTransactionDetails or GetExpressCheckoutDetails
			// GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut
			//$padata =   '&TOKEN='.urlencode($token);
			
			//$httpParsedResponseAr = $this->PPHttpPost('GetExpressCheckoutDetails', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);
	
			if('Completed' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"] || 'Pending' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"])
			{
			    $customerData = Mage::getSingleton('customer/session')->getCustomer();
			    $cdata=Mage::getSingleton('core/session')->getContributeTo();
			    
			    $data = array('user_id'=>$customerData->getId(),'contributed_to'=>$cdata['entity_id'],'cdate'=>date("Y-m-d"),"amount"=>urldecode($httpParsedResponseAr["PAYMENTINFO_0_AMT"]),"transaction_id"=>urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]),"gateway"=>"paypal");
			    $model = Mage::getModel('contribution/contribution')->setData($data);
			    $model->save();
			  
			    $conn = Mage::getSingleton('core/resource')->getConnection('core_read'); 
			    $wconn = Mage::getSingleton('core/resource')->getConnection('core_write');
			    // perform sql queries
			    $result = $conn->fetchAll("SELECT * FROM contribution_wallet where user_id=".$cdata['entity_id']);
			   
			    if(count($result)<=0){
			    $wdata = array('user_id'=>$cdata['entity_id'],"amount"=>urldecode($httpParsedResponseAr["PAYMENTINFO_0_AMT"]));
			    $model = Mage::getModel('contribution/wallet')->setData($wdata);
			    $model->save();
				
			    }else{
			    //	$wdata = array("amount"=>urldecode($httpParsedResponseAr["PAYMENTINFO_0_AMT"])+$result['amount']);
			    //$model = Mage::getModel('contribution/wallet')->load()->addFieldToFilter("user_id",array('eq' => $cdata['entity_id']))->addData($wdata);
			    //$model->save();
			    $wconn->query("update contribution_wallet set amount=amount+".urldecode($httpParsedResponseAr["PAYMENTINFO_0_AMT"])." where user_id=".$cdata['entity_id']);
			    }

			   
			   
			   
			   
			     Mage::getSingleton('core/session')->addSuccess('Thanks for you contribution. Your payment is successful.');
			    $this->_redirectUrl(Mage::getUrl("contribution/index/success"));
			    
			    
			} else  {
			    
			     Mage::getSingleton('core/session')->addError('Your payment failed.');
			    $this->_redirectUrl(Mage::getUrl("contribution/index/cancel"));
	
			}
	   
	    }else{
		    
		    
		     Mage::getSingleton('core/session')->addError('Your payment failed.');
		    $this->_redirectUrl(Mage::getUrl("contribution/index/cancel"));
	    }
	}
	$this->renderLayout(); 
	
    }
    public function successAction(){
	
	//payment gateway success action
	
	$this->loadLayout();   
	$this->getLayout()->getBlock("head")->setTitle($this->__("Wishlist Contribution success"));
	$breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
	 
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("Contribution Cancelled", array(
                "label" => $this->__("Wishlist Contribution completed"),
                "title" => $this->__("Wishlist Contribution completed")
		   ));
    
      $this->renderLayout(); 
    }
    
    public function cancelAction(){
	//payment gateway fail action
	
	$this->loadLayout();   
	$this->getLayout()->getBlock("head")->setTitle($this->__("Wishlist Contribution cancelled"));
	$breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
	 
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("Contribution Cancelled", array(
                "label" => $this->__("Wishlist Contribution failed"),
                "title" => $this->__("Wishlist Contribution failed")
		   ));
    
      $this->renderLayout(); 
    }
    
    
    
    private function PPHttpPost($methodName_, $nvpStr_, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode) {
	
	//send request to paypal gateway
	
	
			$API_UserName = urlencode($PayPalApiUsername);
			$API_Password = urlencode($PayPalApiPassword);
			$API_Signature = urlencode($PayPalApiSignature);
			
			$paypalmode = ($PayPalMode=='sandbox') ? '.sandbox' : '';
	
			echo $API_Endpoint = "https://api-3t".$paypalmode.".paypal.com/nvp";
			$version = urlencode('109.0');
		
			// Set the curl parameters.
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
		
			// Turn off the server and peer verification (TrustManager Concept).
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
		
			
			echo $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
		
		
			curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
		
			
			$httpResponse = curl_exec($ch);
		
			if(!$httpResponse) {
				exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
			}
		
			
			$httpResponseAr = explode("&", $httpResponse);
		
			$httpParsedResponseAr = array();
			foreach ($httpResponseAr as $i => $value) {
				$tmpAr = explode("=", $value);
				if(sizeof($tmpAr) > 1) {
					$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
				}
			}
		
			if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
				exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
			}
		
		return $httpParsedResponseAr;
	}
	
    public function redeemAction(){
	//ajax call for redeemption popup in myaccount wishlist page
	
	$amt=$this->getRequest()->getPost("amount");
	$productid=$this->getRequest()->getPost("itemid");
	$qty=$this->getRequest()->getPost("quantity");
	$product=Mage::getModel('catalog/product')->load($productid);
	$price=$product->getPrice();
	$price=$price*$qty;
	$customerData = Mage::getSingleton('customer/session')->getCustomer();
	 
	 
	$model = Mage::getModel('contribution/wallet')->getCollection()->addFieldToFilter("user_id",$customerData->getId())->load();
	
	$walletamt=$model->getAmount();
	if($amt>$price){
	  echo "fail"  ;
	    
	}
	elseif($walletamt < $amt){
	    echo "fail"  ;
	}
	else{
	    Mage::getSingleton('core/session')->setRedeemAmt($amt);
	//echo Mage::getSingleton('core/session')->getRedeemAmt();
	}
	
    }
    
    
    
    
     public function processauthAction(){
	
	//precess credit card for authorize.net
	
	if (Mage::getSingleton('customer/session')->isLoggedIn()==0){
	    $session = $this->_getSession();
	    $referer=Mage::getUrl("contribution/index/search");
	    $session->setBeforeAuthUrl($referer);
	    
	    $this->_redirectUrl(Mage::getUrl("customer/account/login"));
	  }
          
           $active=Mage::getStoreConfig('payment/authorizenet/active');
       
        if($active!=1){
            
             $this->_redirectUrl(Mage::getUrl("/"));
        }
	
	$this->loadLayout();  
	
	if($_POST) 
	{
	   
	
	    $ItemName       = $this->getRequest()->getPost("itemname"); //Item Name
	    $ItemPrice      = $this->getRequest()->getPost("itemprice"); //Item Price
	    $ItemNumber     = $this->getRequest()->getPost("itemnumber"); //Item Number
	    $ItemDesc       = $this->getRequest()->getPost("itemdesc"); //Item description
	    $ItemQty        = $this->getRequest()->getPost("itemQty"); // Item Quantity
	    $firstname	    = $this->getRequest()->getPost("firstname"); 
	    $lastname	    = $this->getRequest()->getPost("lastname");
	    $street	    = $this->getRequest()->getPost("street");
	    $state	    = $this->getRequest()->getPost("state");
	    $zip	    = $this->getRequest()->getPost("zip");
	    $ItemTotalPrice = ($ItemPrice*$ItemQty); //(Item Price x Quantity = Total) Get total amount of product;
	    
	    //credit card details
	    $card_no=$this->getRequest()->getPost("cc_number");
	    $exp_month=$this->getRequest()->getPost("exp_month");
	    if($exp_month<9)
	    {
		$exp_month='0'.$exp_month;
	    }
	    $exp_year=$this->getRequest()->getPost("exp_year");
	    $exp_year=substr($exp_year,2,2);
	   

		$post_url =  Mage::getStoreConfig('payment/authorizenet/cgi_url');
		//echo Mage::getStoreConfig('payment/authorizenet/trans_key');
		//echo '<br />'.Mage::getStoreConfig('payment/authorizenet/login');
		$post_values = array(
			
			// the API Login ID and Transaction Key must be replaced with valid values
			"x_login"		=> Mage::getStoreConfig('payment/authorizenet/login'),
			"x_tran_key"		=> Mage::getStoreConfig('payment/authorizenet/trans_key'),
		
			"x_version"		=> "3.1",
			"x_delim_data"		=> "TRUE",
			"x_delim_char"		=> "|",
			"x_relay_response"	=> "FALSE",
		
			"x_type"		=> "AUTH_CAPTURE",
			"x_method"		=> "CC",
			"x_card_num"		=> $card_no,
			"x_exp_date"		=> $exp_month.$exp_year,
		
			"x_amount"		=> $ItemTotalPrice,
			"x_description"		=> $ItemDesc,
		
			"x_first_name"		=> $firstname,
			"x_last_name"		=> $lastname,
			"x_address"		=> $street,
			"x_state"		=> $state,
			"x_zip"			=> $zip
			
		);
		
		
		$post_string = "";
		foreach( $post_values as $key => $value )
			{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
		$post_string = rtrim( $post_string, "& " );
		
		
		$request = curl_init($post_url); 
			curl_setopt($request, CURLOPT_HEADER, 0); 
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); 
			curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); 
			$post_response = curl_exec($request); 
			
			
		curl_close ($request); // close curl object
		
		
		$response_array = explode($post_values["x_delim_char"],$post_response);
		//echo $post_url;
		//print_r($response_array);
		//print_r($post_values );exit;
		
		
		    if($response_array[2]==1){
		    
		    
		    $customerData = Mage::getSingleton('customer/session')->getCustomer();
				$cdata=Mage::getSingleton('core/session')->getContributeTo();
				
				$data = array('user_id'=>$customerData->getId(),'contributed_to'=>$cdata['entity_id'],'cdate'=>date("Y-m-d"),"amount"=>urldecode($response_array[9]),"transaction_id"=>urldecode($response_array[37]),"gateway"=>"authorize");
				$model = Mage::getModel('contribution/contribution')->setData($data);
				$model->save();
				
				$conn = Mage::getSingleton('core/resource')->getConnection('core_read'); 
				$wconn = Mage::getSingleton('core/resource')->getConnection('core_write');
				// perform sql queries
				$result = $conn->fetchAll("SELECT * FROM contribution_wallet where user_id=".$cdata['entity_id']);
			       
				if(count($result)<=0){
				$wdata = array('user_id'=>$cdata['entity_id'],"amount"=>urldecode($response_array[9]));
				$model = Mage::getModel('contribution/wallet')->setData($wdata);
				$model->save();
				    
				}else{
				
				$wconn->query("update contribution_wallet set amount=amount+".urldecode($response_array[9])." where user_id=".$cdata['entity_id']);
				}
				
			Mage::getSingleton('core/session')->addSuccess('Thanks for you contribution. Your payment is successful.');
			$this->_redirectUrl(Mage::getUrl("contribution/index/success"));
	       
		    }else{
			
			Mage::getSingleton('core/session')->addError('Your payment failed.');
			    $this->_redirectUrl(Mage::getUrl("contribution/index/cancel"));
		    }
	   	}
	$this->renderLayout(); 
	
    }
    
    public function redeemcheckoutAction(){
	
	//ajax call for redeemption popup in checkout page
	$amt=$this->getRequest()->getPost("amount");
	$price=$this->getRequest()->getPost("orderamount");
	$customerData = Mage::getSingleton('customer/session')->getCustomer();
	 
	 
	$model = Mage::getModel('contribution/wallet')->getCollection()->addFieldToFilter("user_id",$customerData->getId())->getData();
	
	$walletamt=$model[0]['amount'];
	if($amt>$price){
	  echo "fail"  ;
	    
	}
	elseif($walletamt < $amt){
	    echo "fail"  ;
	}
	else{
	    Mage::getSingleton('core/session')->setRedeemAmt($amt);
	echo Mage::getSingleton('core/session')->getRedeemAmt();
	}
    }
    
    public function getcartAction(){
	//$this->loadLayout();
	//echo $this->getLayout()->createBlock('checkout/cart_sidebar')->setTemplate('checkout/cart/minicart/items.phtml')->toHtml();
	
	// echo $this->getLayout()->createBlock('checkout/cart_minicart')->setTemplate('checkout/cart/minicart.phtml')->toHtml();
	echo $count = Mage::helper('checkout/cart')->getSummaryCount(); 
exit;
    }
}