<?php

class Uipl_Faq_Block_Adminhtml_Faqcategory_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("faqcategoryGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("faq/faqcategory")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("faq")->__("ID"),
				"align" =>"right",
				"width" => "50px",
				"type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("category", array(
				"header" => Mage::helper("faq")->__("Category"),
				"index" => "cat_name",
				));
				$this->addColumn('status', array(
				'header' => Mage::helper('faq')->__('Status'),
				'index' => 'status',
				'type' => 'options',
				'options'=>Uipl_Faq_Block_Adminhtml_Faqcategory_Grid::getOptionArray2(),				
				));
						
				$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
				$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_faqcategory', array(
					 'label'=> Mage::helper('faq')->__('Remove Faq Category'),
					 'url'  => $this->getUrl('*/adminhtml_faqcategory/massRemove'),
					 'confirm' => Mage::helper('faq')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray2()
		{
		$data_array=array(); 
			$data_array[0]='0';
			$data_array[1]='1';
		return($data_array);
		}
		static public function getValueArray2()
		{
		$data_array=array();
			foreach(Uipl_Faq_Block_Adminhtml_Faqcategory_Grid::getOptionArray2() as $k=>$v){
				$data_array[]=array('value'=>$k,'label'=>$v);		
			}
		return($data_array);

		}
		
		static public function getValueArray22()
		{
				return array(now());
		}
}