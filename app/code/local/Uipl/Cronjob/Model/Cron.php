<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Module
 * @Author	Ujjal Dutta
 * @Author url	http://www.w3clouds.com
 * @eMail        <ujjal.dutta.pro@gmail.com>
 * @package     Mage_Connect
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Uipl_Cronjob_Model_Cron{	
	public function SendPlanEarlyAlert(){
		//do something
            
           $collection = Mage::getModel('catalog/product')
                ->getCollection()
               ->joinField(
		     'qty',
		     'cataloginventory/stock_item',
		     'qty',
		     'product_id=entity_id',
		     '{{table}}.stock_id=1',
		     'left'
		 )
		 ->addAttributeToFilter('qty', array('lteq' => 0))
		
		;
		
		foreach ($collection as $products)
		{
		    $product = Mage::getModel('catalog/product')->load($products->getId());
			$price=$product->getPrice();
			$special_price=($price*.75);
			
			$product->setShippingDeliveryDays(70);
			$product->setSpecialPrice($special_price);
			$product->save();
		}
		
            
                $orders = Mage::getModel('sales/order')->getCollection()
                    ->addFieldToFilter('status', 'pending')
                    ->addAttributeToSelect('customer_email')
                    ;
                foreach ($orders as $order) {
                    $email = $order->getCustomerEmail();
                    //Mage::log($email . "\n");
                    Mage::log(var_export($email, TRUE),NULL,'planearly.log');
                }
               
	} 
}